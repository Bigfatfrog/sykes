#Sykes Test

The original version was written when I applied to Sykes in February 2019 without using a framework to should that I 
understood MVC. I was offered a position - but didn't accept.

This has now been refactored using Laravel which has the following advantages:-

-Improved security
CSRF protection is built in and request/response data is automatically filtered
-Dependency Injection
This is set up and used here to inject a singleton PropertyService into the controller
-PHPUNIT
Some tests have been wirtten form the service and models.
-Eloquent
The querybuilder has been used and the mySql simplified from the original version

##Installation
The project uses Composer (getcomposer.org) to install it's dependencies.
A .env file is required with the APP_URL and DB connection paratmeters
The web server entry is /public
