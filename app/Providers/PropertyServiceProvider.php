<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Property\Connection;

class PropertyServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            Connection::class,
            function ($app) {
                return new Connection(config('Property'));
            }
        );
    }
}
