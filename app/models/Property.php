<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = "properties";
    protected $primaryKey = '__pk';

    /**
     * Get the bookings
     */
    public function bookings()
    {
        return $this->hasMany('App\Models\Booking', '_fk_property');
    }

    /**
     * Get the bookings
     */
    public function location()
    {
        return $this->hasOne('App\Models\Location', '_fk_location');
    }
}
