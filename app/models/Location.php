<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = "locations";
    protected $primaryKey = '__pk';

    protected $fillable = ['__pk', 'location_name'];
}
