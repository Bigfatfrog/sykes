<?php

namespace App\Http\Controllers;

use App\Services\PropertyService;
use Illuminate\Http\Request;

class PropertiesController extends Controller
{
    public function __construct(PropertyService $propertyService)
    {
        // parent::__construct();
        $this->propertyService = $propertyService;
    }

    public function properties_search(Request $request)
    {
        $search = $request->input();

        $results = $this->propertyService->search($search);
        $resultsArray = $results->toArray();

        $data =
            [
                'properties' => $resultsArray['data'],
                'links' => $results->appends(request()->input())->links(),
                'search' => $search
            ];

        return response()->view('properties', $data, 200);
    }
}
