<?php


namespace App\Services;


use App\Models\Property;
use DB;

class PropertyService
{

    /**
     * Search for a property and bookings in the database
     *
     * @param array $search the search parameters
     * @return object $properties Laravel pagination object of suitable properies
     */
    public function search(array $search = [])
    {
        $this->beach = isset($search['beach']) ? $search['beach'] : null;
        $this->pets = isset($search['pets']) ? $search['pets'] : null;
        $this->sleeps = isset($search['sleeps']) ? $search['sleeps'] : 0;
        $this->beds = isset($search['beds']) ? $search['beds'] : 0;

        $this->from = isset($search['from']) ? $search['from'] : null;
        $this->to = isset($search['to']) ? $search['to'] : null;

        $this->location = isset($search['location']) ? $search['location'] : null;

        $from = !is_null($this->to) ? $this->to : $this->from;
        $to = !is_null($this->from) ? $this->from : $this->to;

        $properties = Property::select(
            'properties.*',
            'locations.location_name',
            DB::raw('count(bookings._fk_property) as bookings_count')
        )
            ->with('bookings')
            ->where('beds', '>=', $this->beds)
            ->where('sleeps', '>=', $this->sleeps)
            ->join('locations', 'properties._fk_location', 'locations.__pk')
            ->leftJoin(
                'bookings',
                function ($join) use ($from, $to) {
                    $join->on('bookings._fk_property', '=', 'properties.__pk');
                    if (!is_null($from)) {
                        $join->on('start_date', '<', DB::raw("'" . $from . "'"));
                        //        $join->on('end_date', '>', \DB::raw("'" . $from . "'"));
                    }
                    if (!is_null($to)) {
                        //         $join->on('start_date', '<', \DB::raw("'" . $to . "'"));
                        $join->on('end_date', '>', DB::raw("'" . $to . "'"));
                    }
                }
            );

        if ($this->beach == true) {
            $properties = $properties->where('near_beach', '=', 1);
        }
        if ($this->pets == true) {
            $properties = $properties->where('accepts_pets', '=', 1);
        }
        if (!is_null($this->location) && $this->location != "") {
            $properties = $properties->where('location_name', 'LIKE', '%' . $this->location . '%');
        }
        $properties->groupBy('properties.__pk', 'bookings._fk_property');

        $properties = $properties->paginate(2);

        return $properties;
    }
}
