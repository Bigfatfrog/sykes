@if(isset($links))
    <div class="row">
        <div class="w-100">
            {!! $links !!}
        </div>
    </div>
@endif
