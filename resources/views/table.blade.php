@if(count($properties) > 0)
    <table class="table table-striped">
        <thead>
        <tr>
            @foreach ($properties[0] as $key => $node)
                @if (substr($key,0,1) === '_' || is_array($node))
                    @continue
                @endif

                <th>{{ucwords(str_replace("_", " ", $key))}}</th>
            @endforeach
        </tr>
        </thead>

        @foreach($properties as $property)
            <tr>
                @foreach ($property as $key => $node)
                    @if (substr($key,0,1) === '_' || is_array($node))
                        @continue
                    @elseif($key === 'accepts_pets' || $key === 'near_beach')
                        <td>{{$node==1?'Yes':'No'}}</td>
                    @elseif($key === 'bookings_count' && (!is_null($search['from']) ||!is_null($search['to'])) )
                        <td>{{$node>0?'UNAVAILABLE':'Available'}}</td>
                    @else
                        <td>{{$node}}</td>
                    @endif
                @endforeach
            </tr>
        @endforeach
    </table>
@else
    <div class="alert alert-danger"><strong>Error!</strong> No records found</div>
@endif

