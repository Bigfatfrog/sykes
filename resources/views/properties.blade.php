<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('header')
<body>
<div class='container'>
    <div class="row">
        <h1>Property Search</h1>
    </div>
    <div class="row">
        <div class="col-12 col-sm-4">
            @include('search')
        </div>
        <div class="col-12 col-sm-8">
            @include('table')
            @include('pagination')
        </div>
    </div>
</div>
</body>
@include('footer')
