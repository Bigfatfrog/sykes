<div class="container">
    <div class="card card-default">
        <div class="card-body">

            <form action="{{env('APP_URL')}}/" method="post">
                @csrf
                <div class="form-group">
                    <label>Availability From:</label>
                    <input type="date" id="from" name="from" max="2030-01-01"
                           min="2017-01-01" class="form-control" value="{{isset($search['from'])?$search['from']:""}}">
                </div>
                <div class="form-group">
                    <label>Availabilty To:</label>
                    <input type="date" id="to" name="to" max="2030-01-01"
                           min="2017-01-01" class="form-control" value="{{isset($search['to'])?$search['to']:""}}">
                </div>

                <div class="form-group">
                    <label for="sel1">Min Bedrooms:</label>
                    <select class="form-control" id="beds" name="beds">
                        <option value="0"
                                @if (isset($search['beds']) && $search['beds'] == 0) selected="selected" @endif >0
                        </option>
                        <option value="1"
                                @if (isset($search['beds']) && $search['beds'] == 1) selected="selected" @endif >1
                        </option>
                        <option value="2"
                                @if (isset($search['beds']) && $search['beds'] == 2) selected="selected" @endif >2
                        </option>
                        <option value="3"
                                @if (isset($search['beds']) && $search['beds'] == 3) selected="selected" @endif >3
                        </option>
                        <option value="4"
                                @if (isset($search['beds']) && $search['beds'] == 4) selected="selected" @endif >4
                        </option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="sel1">Sleeps:</label>
                    <select class="form-control" id="sleeps" name="sleeps">
                        <option value="0"
                                @if (isset($search['sleeps']) && $search['sleeps'] == 0) selected="selected" @endif >0
                        </option>
                        <option value="1"
                                @if (isset($search['sleeps']) && $search['sleeps'] == 1) selected="selected" @endif >1+
                        </option>
                        <option value="2"
                                @if (isset($search['sleeps']) && $search['sleeps'] == 2) selected="selected" @endif >2+
                        </option>
                        <option value="3"
                                @if (isset($search['sleeps']) && $search['sleeps'] == 3) selected="selected" @endif >3+
                        </option>
                        <option value="4"
                                @if (isset($search['sleeps']) && $search['sleeps'] == 4) selected="selected" @endif >4+
                        </option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="pwd">Location:</label>
                    <input type="text" class="form-control" id="location" placeholder="Search for location"
                           name="location" value="{{isset($search['location'])?$search['location']:""}}">
                </div>
                <div class="checkbox">
                    <label><input type="checkbox" name="beach" id="beach"
                                  value="1" {{isset($search['beach'])&&$search['beach']==1?'checked':""}}>
                        Near Beach</label>
                </div>
                <div class="checkbox">
                    <label><input type="checkbox" name="pets" id="pets"
                                  value="1" {{isset($search['pets'])&&$search['pets']==1?'checked':""}}>
                        Accepts Pets</label>
                </div>
                <a class="btn btn-secondary" href=""{{env('APP_URL')}}/"">Clear</a>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
