<?php

namespace Tests\Unit;

use App\Models\Property;
use Tests\TestCase;

class PropertyTest extends TestCase
{
    /**
     * Test the property model.
     *
     * @return void
     */


    public function test_instance()
    {
        $property = new Property();
        $this->assertInstanceOf('App\Models\Property', $property);
    }

    public function test_get()
    {
        $properties = Property::all();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $properties);
    }
}
