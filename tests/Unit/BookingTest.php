<?php

namespace Tests\Unit;

use App\Models\Booking;
use Tests\TestCase;

class BookingTest extends TestCase
{
    /**
     * Test the booking model.
     *
     * @return void
     */


    public function test_instance()
    {
        $booking = new Booking();
        $this->assertInstanceOf('App\Models\Booking', $booking);
    }

    public function test_get()
    {
        $bookings = Booking::all();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $bookings);
    }
}
