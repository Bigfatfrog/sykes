<?php

namespace Tests\Unit;

use App\Models\Booking;
use App\Models\Location;
use App\Models\Property;
use Tests\TestCase;


class PropertyServiceTest extends TestCase
{
    /**
     * Test the booking model.
     *
     * @return void
     */

    public function test_search()
    {
        // set up some test data
        $name = "test " . date('Y-m-d H:m:s');
        $location = new Location();
        $location->location_name = $name;
        $location->save();

        $property = new Property();
        $property->_fk_location = $location->__pk;
        $property->property_name = $name;
        $property->save();

        $booking = new Booking();
        $booking->start_date = '2017-08-26';
        $booking->end_date = '2017-09-02';
        $booking->_fk_property = $property->__pk;
        $booking->save();

        $propertyService = app()->make('App\Services\PropertyService');
        $results = $propertyService->search();
        $this->assertInstanceOf('Illuminate\Pagination\LengthAwarePaginator', $results);

        $property->update(['near_beach' => 1]);
        $results = $propertyService->search(['beach' => 1]);
        foreach ($results as $result) {
            $this->assertEquals(true, $result->near_beach);
        }

        $property->update(['accepts_pets' => 1]);
        $results = $propertyService->search(['pets' => 1]);
        foreach ($results as $result) {
            $this->assertEquals(true, $result->accepts_pets);
        }

        $property->update(['sleeps' => 5]);
        $results = $propertyService->search(['sleeps' => 5]);
        foreach ($results as $result) {
            $this->assertEquals(true, $result->sleeps >= 5);
        }

        $property->update(['beds' => 6]);
        $results = $propertyService->search(['beds' => 6]);
        foreach ($results as $result) {
            $this->assertEquals(true, $result->beds >= 6);
        }

        $results = $propertyService->search(['location' => $name]);
        foreach ($results as $result) {
            $this->assertEquals(
                true,
                stripos($result->location_name, $name) !== false,
                $result->location_name . " doesn't contain $name"
            );
        }

        $from = '2017-08-01';
        $results = $propertyService->search(['location' => $name, 'from' => $from]);

        $this->assertEquals(true, isset($results[0]));
        $this->assertEquals($property->__pk, $results[0]->__pk);
        $this->assertEquals(0, $results[0]->bookings_count);

        $from = '2017-10-01';
        $results = $propertyService->search(['location' => $name, 'from' => $from]);
        $this->assertEquals(true, isset($results[0]));
        $this->assertEquals($property->__pk, $results[0]->__pk);
        $this->assertEquals(0, $results[0]->bookings_count);

        $from = '2017-09-01';
        $results = $propertyService->search(['location' => $name, 'from' => $from]);
        $this->assertEquals(true, isset($results[0]));
        $this->assertEquals($property->__pk, $results[0]->__pk);
        $this->assertEquals(1, $results[0]->bookings_count);

        $to = '2017-08-01';
        $results = $propertyService->search(['location' => $name, 'to' => $to]);

        $this->assertEquals(true, isset($results[0]));
        $this->assertEquals($property->__pk, $results[0]->__pk);
        $this->assertEquals(0, $results[0]->bookings_count);

        $to = '2017-10-01';
        $results = $propertyService->search(['location' => $name, 'to' => $to]);
        $this->assertEquals(true, isset($results[0]));
        $this->assertEquals($property->__pk, $results[0]->__pk);
        $this->assertEquals(0, $results[0]->bookings_count);

        $to = '2017-09-01';
        $results = $propertyService->search(['location' => $name, 'to' => $to]);
        $this->assertEquals(true, isset($results[0]));
        $this->assertEquals($property->__pk, $results[0]->__pk);
        $this->assertEquals(1, $results[0]->bookings_count);

        $from = '2017-07-25';
        $to = '2017-08-01';
        $results = $propertyService->search(['location' => $name, 'from' => $from, 'to' => $to]);

        $this->assertEquals(true, isset($results[0]));
        $this->assertEquals($property->__pk, $results[0]->__pk);
        $this->assertEquals(0, $results[0]->bookings_count);

        $from = '2017-09-28';
        $to = '2017-10-01';
        $results = $propertyService->search(['location' => $name, 'from' => $from, 'to' => $to]);
        $this->assertEquals(true, isset($results[0]));
        $this->assertEquals($property->__pk, $results[0]->__pk);
        $this->assertEquals(0, $results[0]->bookings_count);

        $from = '2017-08-31';
        $to = '2017-09-01';
        $results = $propertyService->search(['location' => $name, 'from' => $from, 'to' => $to]);
        $this->assertEquals(true, isset($results[0]));
        $this->assertEquals($property->__pk, $results[0]->__pk);
        $this->assertEquals(1, $results[0]->bookings_count);

        $from = '2017-07-31';
        $to = '2017-10-01';
        $results = $propertyService->search(['location' => $name, 'from' => $from, 'to' => $to]);
        $this->assertEquals(true, isset($results[0]));
        $this->assertEquals($property->__pk, $results[0]->__pk);
        $this->assertEquals(1, $results[0]->bookings_count);

        //tidy up data
        $property->delete();
        $booking->delete();
        $location->delete();
    }

}
